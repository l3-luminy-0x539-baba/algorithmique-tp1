import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

private static Graph gt;
public static void main (String[] args) {
	Graph <String> graphe = lireFichier("formule-2-sat.txt");
	System.out.println(graphe.toString());
	ArrayList<ArrayList<Integer>> composantesConnexes = kosaraju(graphe);
	System.out.println(gt.toString());
}

public static Graph<String> lireFichier(String nom_fichier) {
	Graph<String> graph = null;
	try {
		File file = new File(nom_fichier);
		Scanner sc = new Scanner(file);
		int taille = sc.nextInt() * 2 + 1;
		graph = new Graph <String> (taille);
		while (sc.hasNext()) {
			int a = sc.nextInt();
			int b = sc.nextInt();
			graph.addArc(versSommet(a * -1, taille), versSommet(b, taille), a * -1 + " -> " + b);
		}
		sc.close();
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return graph;
}
public static int versSommet(int n, int taille) {
	return n + taille/2;
}

public static int versLitteral(int n, int taille) {
	return n - taille/2;
}

public static ArrayList<ArrayList<Integer>> kosaraju(Graph<String> graphe) {
	gt = new Graph <String> (graphe.order());
	//construction graphe transpose
	graphe.iterEdges(new Graph.ArcConsumer<Object>() {
		public void apply(int source, int dest, Object label) {
			gt.addArc(dest, source, versLitteral(dest,gt.order()) + 
					" -> " + versLitteral(source,gt.order()));
		}
	});
	return null;
}
}